#include "CelestialBody.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/trigonometric.hpp"


CelestialBody::CelestialBody(bonobo::mesh_data const& shape, GLuint const* program, GLuint diffuse_texture_id) {
	_body.set_geometry(shape);
	_body.set_program(program);
	_body.add_texture("diffuse_texture", diffuse_texture_id, GL_TEXTURE_2D);
}



glm::mat4 CelestialBody::render(std::chrono::microseconds ellapsed_time, glm::mat4 const& view_projection, glm::mat4 const& parent_transform)
{

	std::chrono::duration<float> const ellapsed_time_s = ellapsed_time;
	_spin_angle += ellapsed_time_s.count() * _rotation_speed;
	_orbit_angle += ellapsed_time_s.count() * _orbit_speed;
	
	glm::mat4 identity_matrix = glm::mat4(1.0f);

	//Scaling
	glm::mat4 scaling_matrix = glm::scale(identity_matrix, _body_scale);

	//Spin
	glm::mat4 spin_matrix = glm::rotate(identity_matrix, _spin_angle, glm::vec3(0,1,0));
	glm::mat4 tilt_matrix = glm::rotate(identity_matrix, _inclination, glm::vec3(0, 0, 1));
	glm::mat4 final_spin_matrix = tilt_matrix * spin_matrix;

	//Orbit 
	glm::mat4 orbit_matrix = identity_matrix;
	glm::mat4 orbit_rotation_matrix = glm::rotate(identity_matrix, _orbit_angle, glm::vec3(0, 1, 0));
	glm::mat4 translation_matrix = glm::translate(identity_matrix,glm::vec3(_radius, 0, 0));
	glm::mat4 orbit_tilt_matrix = glm::rotate(identity_matrix, _orbit_inclination, glm::vec3(0, 0, 1));
	orbit_matrix = orbit_tilt_matrix * orbit_rotation_matrix * translation_matrix;


	//Rings
	glm::mat4 rings_scaling_matrix = glm::scale(identity_matrix, glm::vec3(_rings_scale.x, _rings_scale.y, 1));
	glm::mat4 rings_rotation_matrix = glm::rotate(identity_matrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
	glm::mat4 final_ring_matrix = rings_scaling_matrix * rings_rotation_matrix;

	//Setting it together and rendering
	_body.render(view_projection, parent_transform * orbit_matrix * final_spin_matrix * scaling_matrix);
	_rings.render(view_projection, orbit_matrix * final_ring_matrix);

	return orbit_matrix;
}

void CelestialBody::set_scale(glm::vec3 const& scale)
{
	_body_scale = scale;
}

void CelestialBody::set_spin(SpinConfiguration configuration)
{
	_rotation_speed = configuration.speed;
	_inclination = configuration.inclination;
}

void CelestialBody::set_orbit(OrbitConfiguration const& configuration)
{
	_radius = configuration.radius;
	_orbit_inclination = configuration.inclination;
	_orbit_speed = configuration.speed;
}

void CelestialBody::set_ring(bonobo::mesh_data const& shape, GLuint const* program, GLuint diffuse_texture_id, glm::vec2 const& scale)
{
	_rings.set_geometry(shape);
	_rings.set_program(program);
	_rings.add_texture("diffuse_texture", diffuse_texture_id, GL_TEXTURE_2D);
	_rings_scale = scale;
}

void CelestialBody::add_child(CelestialBody* child)
{
	_children.push_back(child);
}

std::vector<CelestialBody*> const& CelestialBody::get_children() const
{
	return _children;
}








