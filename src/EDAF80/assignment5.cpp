#include "assignment5.hpp"

#include "config.hpp"
#include "core/Bonobo.h"
#include "core/FPSCamera.h"
#include "core/helpers.hpp"
#include "core/ShaderProgramManager.hpp"
#include "parametric_shapes.hpp"
#include "core/node.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <imgui.h>
#include <tinyfiledialogs.h>
#include <sstream>
#include <list>

#include <stdexcept>
#include <cstdlib>

edaf80::Assignment5::Assignment5(WindowManager& windowManager) :
	mCamera(0.5f * glm::half_pi<float>(),
		static_cast<float>(config::resolution_x) / static_cast<float>(config::resolution_y),
		0.01f, 1000.0f),
	inputHandler(), mWindowManager(windowManager), window(nullptr)
{
	WindowManager::WindowDatum window_datum{ inputHandler, mCamera, config::resolution_x, config::resolution_y, 0, 0, 0, 0 };

	window = mWindowManager.CreateGLFWWindow("EDAF80: Assignment 5", window_datum, config::msaa_rate);
	if (window == nullptr) {
		throw std::runtime_error("Failed to get a window: aborting!");
	}


}

void
edaf80::Assignment5::run()
{
	// Set up the camera
	mCamera.mWorld.SetTranslate(glm::vec3(0, 0, 20));
	mCamera.mMouseSensitivity = 0.003f;
	mCamera.mMovementSpeed = 4.0f; // 3 m/s => 10.8 km/h

	auto camera_position = mCamera.mWorld.GetTranslation();
	float ellapsed_time_s = 0.0f;
	int current_level = 1;

	// Create the shader programs
	ShaderProgramManager program_manager;
	GLuint fallback_shader = 0u;
	program_manager.CreateAndRegisterProgram("Fallback",
		{ { ShaderType::vertex, "EDAF80/fallback.vert" },
		  { ShaderType::fragment, "EDAF80/fallback.frag" } },
		fallback_shader);
	if (fallback_shader == 0u) {
		LogError("Failed to load fallback shader");
		return;
	}

	GLuint skybox_shader = 0u;
	program_manager.CreateAndRegisterProgram("Skybox",
		{ { ShaderType::vertex, "EDAF80/skybox.vert" },
		  { ShaderType::fragment, "EDAF80/skybox.frag" } },
		skybox_shader);

	if (skybox_shader == 0u) {
		LogError("Failed to load skybox shader");
	}

	GLuint water_shader = 0u;
	program_manager.CreateAndRegisterProgram("Water",
		{ { ShaderType::vertex, "EDAF80/water.vert" },
		  { ShaderType::fragment, "EDAF80/water.frag" } },
		water_shader);

	if (water_shader == 0u)
		LogError("Failed to load diffuse shader");

	GLuint phong_shader = 0u;
	program_manager.CreateAndRegisterProgram("Phong",
		{ { ShaderType::vertex, "EDAF80/phong.vert" },
		  { ShaderType::fragment, "EDAF80/phong.frag" } },
		phong_shader);

	if (phong_shader == 0u) {
		LogError("Failed to load phong shader");
	}

	GLuint grow_shader = 0u;
	program_manager.CreateAndRegisterProgram("Grow",
		{ { ShaderType::vertex, "EDAF80/grow.vert" },
		  { ShaderType::fragment, "EDAF80/grow.frag" } },
		grow_shader);

	if (grow_shader == 0u) {
		LogError("Failed to load grow shader");
	}


	//
	//Create uniforms
	//

	auto light_position = glm::vec3(20.0f, 16.0f, 10.0f);
	auto const set_uniforms = [&light_position](GLuint program) {
		glUniform3fv(glGetUniformLocation(program, "light_position"), 1, glm::value_ptr(light_position));
	};

	bool use_normal_mapping = true;
	camera_position = mCamera.mWorld.GetTranslation();
	auto const water_set_uniforms = [&use_normal_mapping, &light_position, &camera_position, &ellapsed_time_s](GLuint program) {
		glUniform1i(glGetUniformLocation(program, "use_normal_mapping"), use_normal_mapping ? 1 : 0);
		glUniform3fv(glGetUniformLocation(program, "light_position"), 1, glm::value_ptr(light_position));
		glUniform3fv(glGetUniformLocation(program, "camera_position"), 1, glm::value_ptr(camera_position));
		glUniform1f(glGetUniformLocation(program, "ellapsed_time_s"), ellapsed_time_s);
	};


	auto ambient = glm::vec3(0.1f, 0.1f, 0.1f);
	auto diffuse = glm::vec3(0.7f, 0.2f, 0.4f);
	auto specular = glm::vec3(1.0f, 1.0f, 1.0f);
	auto shininess = 10.0f;
	auto const phong_set_uniforms = [&use_normal_mapping, &light_position, &camera_position, &ambient, &diffuse, &specular, &shininess](GLuint program) {
		glUniform1i(glGetUniformLocation(program, "use_normal_mapping"), use_normal_mapping ? 1 : 0);
		glUniform3fv(glGetUniformLocation(program, "light_position"), 1, glm::value_ptr(light_position));
		glUniform3fv(glGetUniformLocation(program, "camera_position"), 1, glm::value_ptr(camera_position));
		glUniform3fv(glGetUniformLocation(program, "ambient"), 1, glm::value_ptr(ambient));
		glUniform3fv(glGetUniformLocation(program, "diffuse"), 1, glm::value_ptr(diffuse));
		glUniform3fv(glGetUniformLocation(program, "specular"), 1, glm::value_ptr(specular));
		glUniform1f(glGetUniformLocation(program, "shininess"), shininess);
	};


	//Textures
	auto cube_map_id = bonobo::loadTextureCubeMap(config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_+X.png"),
		config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_-X.png"),
		config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_+Y.png"), config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_-Y.png"),
		config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_+Z.png"), config::resources_path("cubemaps/BlueSky/Epic_BlueSunset_-Z.png"));

	auto diffuse_id = bonobo::loadTexture2D(config::resources_path("textures/cobblestone_floor_08_diff_2k.jpg"));
	auto normal_id = bonobo::loadTexture2D(config::resources_path("textures/cobblestone_floor_08_nor_2k.jpg"));
	auto rough_id = bonobo::loadTexture2D(config::resources_path("textures/cobblestone_floor_08_rough_2k.jpg"));
	auto water_normal_id = bonobo::loadTexture2D(config::resources_path("textures/waves.png"));

	auto metal_diffuse_id = bonobo::loadTexture2D(config::resources_path("textures/Metal_Plate_044_BaseColor.jpg"));
	auto metal_rough_id = bonobo::loadTexture2D(config::resources_path("textures/Metal_Plate_044_Roughness.jpg"));
	auto metal_normal_id = bonobo::loadTexture2D(config::resources_path("textures/Metal_Plate_044_Normal.jpg"));

	auto crystal_diffuse_id = bonobo::loadTexture2D(config::resources_path("textures/Crystal_004_basecolor.jpg"));
	auto crystal_rough_id = bonobo::loadTexture2D(config::resources_path("textures/Crystal_004_roughness.jpg"));
	auto crystal_normal_id = bonobo::loadTexture2D(config::resources_path("textures/Crystal_004_normal.jpg"));


	//Shapes
	int lvlWidth = 30;
	int lvlHeight = 10;

	auto skybox_shape = parametric_shapes::createSphere(300.0f, 200u, 200u);
	if (skybox_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the skybox");
		return;
	}

	float projectile_radius = 0.5f;

	auto sphere_shape = parametric_shapes::createSphere(projectile_radius, 40u, 40u);
	if (sphere_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	float player_size_x = 3.0f;
	float player_size_z = 5.0f;

	auto quad_shape = parametric_shapes::createQuad(player_size_x, player_size_z, 10u, 10u);
	if (quad_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	float obs_radius = 0.75f;

	auto const obs_shape = parametric_shapes::createSphere(obs_radius, 10u, 10u);
	if (obs_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	auto water_shape = parametric_shapes::createQuad(1000.0f, 1000.0f, 200u, 200u);
	if (water_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	auto wall_shape_side = parametric_shapes::createBetterQuad(lvlHeight, lvlWidth, glm::vec3(0,0,0), 10, 10,2) ;
	if (wall_shape_side.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	auto roof_shape = parametric_shapes::createBetterQuad(lvlHeight, lvlWidth * 3, glm::vec3(0, 0, 0), 10, 10, 1);
	if (roof_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	//Create the level
	std::vector<std::vector<unsigned int>> tileData;
	std::vector<Node> walls = std::vector<Node>();


	std::string level1_path = config::resources_path("levels/level1.txt");
	std::string level2_path = config::resources_path("levels/level2.txt");
	std::string level3_path = config::resources_path("levels/level3.txt");

	//level1
	tileData = load_level(level1_path.c_str(), lvlWidth, lvlHeight);
	unsigned int height1 = tileData.size();
	unsigned int width1 = tileData[0].size();

	std::vector<Node> level1 = std::vector<Node>();
	std::vector<Node>* obstacles = &level1;
	for (int y = 0; y < height1; ++y) {
		for (int x = 0; x < width1; ++x) {
			if (tileData[y][x] == 1) {
				Node box;
				box.set_geometry(obs_shape);
				box.set_program(&phong_shader, phong_set_uniforms);
				box.add_texture("diffuse_texture", crystal_diffuse_id, GL_TEXTURE_2D);
				box.add_texture("normal_texture", crystal_normal_id, GL_TEXTURE_2D);
				box.add_texture("rough_texture", crystal_rough_id, GL_TEXTURE_2D);
				box.get_transform().SetTranslate(glm::vec3(x * 2, y * 2, 0));
				level1.push_back(box);
			}
		}
	}

	glm::vec3 wall_position = glm::vec3(-3.5, -20, -5);

	Node wall1;
	wall1.set_geometry(wall_shape_side);
	wall1.set_program(&phong_shader, phong_set_uniforms);
	wall1.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	wall1.get_transform().SetTranslate(glm::vec3(wall_position.x,wall_position.y,wall_position.z));
	walls.push_back(wall1);


	//level2
	tileData = load_level(level2_path.c_str(), lvlWidth, lvlHeight);
	unsigned int height2 = tileData.size();
	unsigned int width2 = tileData[0].size();


	std::vector<Node> level2 = std::vector<Node>();
	for (int y = 0; y < height2; ++y) {
		for (int x = 0; x < width2; ++x) {
			if (tileData[y][x] == 1) {
				Node box;
				box.set_geometry(obs_shape);
				box.set_program(&phong_shader, phong_set_uniforms);
				box.add_texture("diffuse_texture", crystal_diffuse_id, GL_TEXTURE_2D);
				box.add_texture("normal_texture", crystal_normal_id, GL_TEXTURE_2D);
				box.add_texture("rough_texture", crystal_rough_id, GL_TEXTURE_2D);
				box.get_transform().SetTranslate(glm::vec3(lvlWidth + x * 2, y * 2, 0));
				level2.push_back(box);
			}
		}
	}

	Node wall2;
	wall2.set_geometry(wall_shape_side);
	wall2.set_program(&phong_shader, phong_set_uniforms);
	wall2.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	wall2.get_transform().SetTranslate(glm::vec3(wall_position.x + lvlWidth, wall_position.y, wall_position.z));
	walls.push_back(wall2);


	//level3
	tileData = load_level(level3_path.c_str(), lvlWidth, lvlHeight);
	unsigned int height3 = tileData.size();
	unsigned int width3 = tileData[0].size();

	std::vector<Node> level3 = std::vector<Node>();
	for (int y = 0; y < height3; ++y) {
		for (int x = 0; x < width3; ++x) {
			if (tileData[y][x] == 1) {
				Node box;
				box.set_geometry(obs_shape);
				box.set_program(&phong_shader, phong_set_uniforms);
				box.add_texture("diffuse_texture", crystal_diffuse_id, GL_TEXTURE_2D);
				box.add_texture("normal_texture", crystal_normal_id, GL_TEXTURE_2D);
				box.add_texture("rough_texture", crystal_rough_id, GL_TEXTURE_2D);
				box.get_transform().SetTranslate(glm::vec3(2 * lvlWidth + x * 2, y * 2, 0));
				level3.push_back(box);
			}
		}
	}

	Node wall3;
	wall3.set_geometry(wall_shape_side);
	wall3.set_program(&phong_shader, phong_set_uniforms);
	wall3.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	wall3.get_transform().SetTranslate(glm::vec3(wall_position.x + lvlWidth * 2, wall_position.y, wall_position.z));
	walls.push_back(wall3);

	Node wall4;
	wall4.set_geometry(wall_shape_side);
	wall4.set_program(&phong_shader, phong_set_uniforms);
	wall4.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	wall4.get_transform().SetTranslate(glm::vec3(wall_position.x + lvlWidth * 3, wall_position.y, wall_position.z));
	walls.push_back(wall4);

	Node roof;
	roof.set_geometry(roof_shape);
	roof.set_program(&phong_shader, phong_set_uniforms);
	roof.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	roof.get_transform().SetTranslate(glm::vec3(wall_position.x, lvlHeight, wall_position.z));


	//Nodes
	Node skybox;
	skybox.set_geometry(skybox_shape);
	skybox.set_program(&skybox_shader, set_uniforms);
	skybox.add_texture("my_cube_map", cube_map_id, GL_TEXTURE_CUBE_MAP);

	Node projectile;
	projectile.set_geometry(sphere_shape);
	projectile.set_program(&grow_shader, water_set_uniforms);
	projectile.add_texture("diffuse_texture", metal_diffuse_id, GL_TEXTURE_2D);
	projectile.add_texture("normal_texture", metal_normal_id, GL_TEXTURE_2D);
	projectile.add_texture("rough_texture", metal_rough_id, GL_TEXTURE_2D);
	projectile.get_transform().SetTranslate(glm::vec3(width1, -lvlHeight + 1, 0));
	float projectile_start_position_x = projectile.get_transform().GetTranslation().x;

	glm::vec3 projDirection = glm::vec3(0, 1, 0);

	Node player;
	player.set_geometry(quad_shape);
	player.set_program(&phong_shader, phong_set_uniforms);
	player.get_transform().SetTranslate(glm::vec3(width1 - 2, -lvlHeight, -1.5f));
	float player_start_position_x = player.get_transform().GetTranslation().x;

	Node water;
	water.set_geometry(water_shape);
	water.set_program(&water_shader, water_set_uniforms);
	water.add_texture("my_cube_map", cube_map_id, GL_TEXTURE_CUBE_MAP);
	water.add_texture("normal_texture", water_normal_id, GL_TEXTURE_2D);
	water.get_transform().SetTranslate(glm::vec3(-300, -lvlHeight * 2, -300));

	mCamera.mWorld.SetTranslate(glm::vec3(width1, -1.0f, 23.0f));


	glClearDepthf(1.0f);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	// Enable face culling to improve performance:
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);
	//glCullFace(GL_BACK);


	auto lastTime = std::chrono::high_resolution_clock::now();
	std::int32_t demo_sphere_program_index = 0;
	auto polygon_mode = bonobo::polygon_mode_t::fill;

	bool show_logs = true;
	bool show_gui = true;
	bool shader_reload_failed = false;
	bool isPaused = true;
	bool isColliding = false;

	while (!glfwWindowShouldClose(window)) {

		auto const nowTime = std::chrono::high_resolution_clock::now();
		auto const deltaTimeUs = std::chrono::duration_cast<std::chrono::microseconds>(nowTime - lastTime);
		lastTime = nowTime;
		ellapsed_time_s += std::chrono::duration<float>(deltaTimeUs).count();
		

		auto& io = ImGui::GetIO();
		inputHandler.SetUICapture(io.WantCaptureMouse, io.WantCaptureKeyboard);

		glfwPollEvents();
		inputHandler.Advance();
		mCamera.Update(deltaTimeUs, inputHandler);
		camera_position = mCamera.mWorld.GetTranslation();

		if (inputHandler.GetKeycodeState(GLFW_KEY_R) & JUST_PRESSED) {
			shader_reload_failed = !program_manager.ReloadAllPrograms();
			if (shader_reload_failed)
				tinyfd_notifyPopup("Shader Program Reload Error",
					"An error occurred while reloading shader programs; see the logs for details.\n"
					"Rendering is suspended until the issue is solved. Once fixed, just reload the shaders again.",
					"error");
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_F3) & JUST_RELEASED)
			show_logs = !show_logs;
		if (inputHandler.GetKeycodeState(GLFW_KEY_F2) & JUST_RELEASED)
			show_gui = !show_gui;
		if (inputHandler.GetKeycodeState(GLFW_KEY_F11) & JUST_RELEASED)
			mWindowManager.ToggleFullscreenStatusForWindow(window);


		// Retrieve the actual framebuffer size: for HiDPI monitors,
		// you might end up with a framebuffer larger than what you
		// actually asked for. For example, if you ask for a 1920x1080
		// framebuffer, you might get a 3840x2160 one instead.
		// Also it might change as the user drags the window between
		// monitors with different DPIs, or if the fullscreen status is
		// being toggled.
		int framebuffer_width, framebuffer_height;
		glfwGetFramebufferSize(window, &framebuffer_width, &framebuffer_height);
		glViewport(0, 0, framebuffer_width, framebuffer_height);

		//Projectile movement and collision check
		float movementSpeed = 0.175f;
		glm::vec3 projPos = projectile.get_transform().GetTranslation();
		glm::vec3 difference;

		//Obstacle collision
		int index = 0;
		int toRemove = -1;
		for (Node c : *obstacles) {
			difference = projPos - c.get_transform().GetTranslation();

			if (std::abs(glm::length(difference)) < projectile_radius + obs_radius) {
				projDirection = reflect(projDirection,-difference);
				toRemove = index;
				//projDirection *= difference;
			}
			index++;
		}

		if (toRemove != -1) {
			obstacles->erase(obstacles->begin() + toRemove);
		}

		//Player quad collision

		if (!isPaused) {
			glm::vec3 playerDim = glm::vec3(player_size_x / 2, 0, player_size_z / 2);
			glm::vec3 playerPos = (player.get_transform().GetTranslation() + playerDim);
			difference = projPos - playerPos;
			//std::cout << (std::abs(difference.y) < projectile_radius);
			if (std::abs(glm::length(glm::vec3(difference.x, 0, difference.z))) < std::abs(glm::length(playerDim) + projectile_radius) && std::abs(difference.y) < projectile_radius) {

				if (!isColliding) {
					glm::vec3 simpleDiff = glm::vec3(difference.x, difference.y, 0);
					projDirection = glm::reflect(simpleDiff, glm::vec3(0, 0, 1));
					//projDirection *= simpleDiff;
				}
				isColliding = true;
			}
			else {
				isColliding = false;
			}


			float projectile_x = projectile.get_transform().GetTranslation().x;
			float projectile_y = projectile.get_transform().GetTranslation().y;
			if (projectile_x - projectile_radius  < projectile_start_position_x - lvlWidth / 2 || projectile_x  > projectile_start_position_x + lvlWidth / 2) {
				projDirection = glm::vec3(-projDirection.x, projDirection.y, 0);
			}
			else if (projectile_y + projectile_radius > lvlHeight) {
				projDirection = glm::vec3(projDirection.x, -projDirection.y, 0);
			}

			glm::vec3 newPos = glm::normalize(projDirection) * movementSpeed * (float)(deltaTimeUs.count() / 10000);
			projectile.get_transform().Translate(newPos);


		}


		//
		// Move players pad
		//
		float movementspeed = 0.25f;

		if (!isPaused) {
			if (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT) & GLFW_REPEAT) {
				if (player.get_transform().GetTranslation().x - player_size_x > player_start_position_x - lvlWidth / 2) {
					player.get_transform().Translate(glm::vec3(-movementspeed, 0, 0));

				}
			}

			if (inputHandler.GetKeycodeState(GLFW_KEY_LEFT) & GLFW_REPEAT) {
				if (player.get_transform().GetTranslation().x + player_size_x < player_start_position_x + lvlWidth / 2) {
					player.get_transform().Translate(glm::vec3(movementspeed, 0, 0));

				}
			}
		}


		//
		//obstacle rotation
		//
		switch (current_level) {
		case 1:
			for (auto &box : level1) {
				box.get_transform().Rotate(glm::radians(5.0f), glm::vec3(rand(), rand(), rand()));
			}
			break;
		case 2:
			for (auto &box: level2) {
				box.get_transform().Rotate(glm::radians(5.0f), glm::vec3(rand(), rand(), rand()));
			}
			break;
		case 3:
			for (auto &box : level3) {
				box.get_transform().Rotate(glm::radians(5.0f), glm::vec3(rand(), rand(), rand()));
			}
			break;
		default:
			break;
		}

		mWindowManager.NewImGuiFrame();

		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		bonobo::changePolygonMode(polygon_mode);


		if (!shader_reload_failed) {
			skybox.render(mCamera.GetWorldToClipMatrix());
			projectile.render(mCamera.GetWorldToClipMatrix());
			player.render(mCamera.GetWorldToClipMatrix());
			water.render(mCamera.GetWorldToClipMatrix());
			roof.render(mCamera.GetWorldToClipMatrix());
			for (auto const& wall : walls) {
				wall.render(mCamera.GetWorldToClipMatrix());
			}

			switch (current_level) {
				case 1:
					for (auto &box : level1) {
						box.render(mCamera.GetWorldToClipMatrix());
					}
					break;
				case 2:
					for (auto &box : level2) {
						box.render(mCamera.GetWorldToClipMatrix());
					}
					break;
				case 3:
					for (auto &box : level3) {
						box.render(mCamera.GetWorldToClipMatrix());
					}
					break;
				default:
					break;
			}
		}


		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		bool opened = ImGui::Begin("Scene Control", nullptr, ImGuiWindowFlags_None);
		if (opened) {
			bonobo::uiSelectPolygonMode("Polygon mode", polygon_mode);
			auto demo_sphere_selection_result = program_manager.SelectProgram("Demo sphere", demo_sphere_program_index);
			if (demo_sphere_selection_result.was_selection_changed) {
				projectile.set_program(demo_sphere_selection_result.program, phong_set_uniforms);
			}
			ImGui::Checkbox("Start/pause game", &isPaused);
			if (ImGui::Button("Level 1") == 1) {
				isPaused = true;
				mCamera.mWorld.SetTranslate(glm::vec3(width1, -1.0f, 23.0f));

				player.get_transform().SetTranslate(glm::vec3(width1 - 2, -lvlHeight, -1.5f));
				player_start_position_x = player.get_transform().GetTranslation().x;

				projectile.get_transform().SetTranslate(glm::vec3(width1, -lvlHeight + 1, 0.0f));
				projectile_start_position_x = projectile.get_transform().GetTranslation().x;
				projDirection = glm::vec3(1,1,0);

				light_position = glm::vec3(20.0f, 16.0f, 10.0f);
				obstacles = &level1;
				current_level = 1;
			}
			if (ImGui::Button("Level 2") == 1) {
				isPaused = true;
				mCamera.mWorld.SetTranslate(glm::vec3(width2 + lvlWidth, -1.0f, 23.0f));

				player.get_transform().SetTranslate(glm::vec3(width2 + lvlWidth - 2, -lvlHeight, -1.5f));
				player_start_position_x = player.get_transform().GetTranslation().x;

				projectile.get_transform().SetTranslate(glm::vec3(width2 + lvlWidth, -lvlHeight + 1, 0.0f));
				projectile_start_position_x = projectile.get_transform().GetTranslation().x;
				projDirection = glm::vec3(1, 1, 0);

				light_position = glm::vec3(20.0f + lvlWidth, 16.0f, 10.0f);
				obstacles = &level2;
				current_level = 2;
			}
			if (ImGui::Button("Level 3") == 1) {
				isPaused = true;
				mCamera.mWorld.SetTranslate(glm::vec3(width3 + lvlWidth * 2, -1.0f, 23.0f));

				player.get_transform().SetTranslate(glm::vec3(width3 + lvlWidth * 2 - 2, -lvlHeight, -1.5f));
				player_start_position_x = player.get_transform().GetTranslation().x;

				projectile.get_transform().SetTranslate(glm::vec3(width3 + lvlWidth * 2, -lvlHeight + 1, 0.0f));
				projectile_start_position_x = projectile.get_transform().GetTranslation().x;
				projDirection = glm::vec3(1, 1, 0);

				light_position = glm::vec3(20.0f + lvlWidth * 2, 16.0f, 10.0f);
				obstacles = &level3;
				current_level = 3;
			}
			ImGui::Separator();
			ImGui::Checkbox("Use normal mapping", &use_normal_mapping);
			/*ImGui::ColorEdit3("Ambient", glm::value_ptr(ambient));
			ImGui::ColorEdit3("Diffuse", glm::value_ptr(diffuse));
			ImGui::ColorEdit3("Specular", glm::value_ptr(specular));
			ImGui::SliderFloat("Shininess", &shininess, 1.0f, 1000.0f);
			//ImGui::SliderFloat3("Light Position", glm::value_ptr(light_position), -20.0f, 20.0f);*/
		}
		ImGui::End();

		opened = ImGui::Begin("Render Time", nullptr, ImGuiWindowFlags_None);
		if (opened)
			ImGui::Text("%.3f ms", std::chrono::duration<float, std::milli>(deltaTimeUs).count());
		ImGui::End();

		if (show_logs)
			Log::View::Render();
		if (show_gui)
			mWindowManager.RenderImGuiFrame();

		glfwSwapBuffers(window);
	}
}

std::vector<std::vector<unsigned int>> edaf80::Assignment5::load_level(const char* file, unsigned int width, unsigned int height)
{

	unsigned int tileCode;
	std::string line;
	std::ifstream fstream(file);
	std::vector<std::vector<unsigned int>> tileData;
	if (fstream)
	{
		while (std::getline(fstream, line)) // read each line from level file
		{
			std::istringstream sstream(line);
			std::vector<unsigned int> row;
			while (sstream >> tileCode) // read each word separated by spaces
				row.push_back(tileCode);
			tileData.push_back(row);
		}
		if (tileData.size() > 0)
			return tileData;
	}
}


int main()
{
	Bonobo framework;

	try {
		edaf80::Assignment5 assignment5(framework.GetWindowManager());
		assignment5.run();
	}
	catch (std::runtime_error const& e) {
		LogError(e.what());
	}
}
