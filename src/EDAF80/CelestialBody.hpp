#pragma once

#include "assignment1.hpp"
#include "../core/helpers.hpp"
#include <chrono>
#include "../external/GLAD/glad/glad.h"
#include "../core/node.hpp"
#include "glm/vec3.hpp"
#include <stack>


class CelestialBody {

	private: Node _body;
	private: Node _rings;
	private: glm::vec3 _body_scale = glm::vec3(1,1,1);
	private: glm::vec2 _rings_scale = glm::vec2(1, 1);
	private: float _spin_angle = 0,_orbit_angle = 0;
	private: float _rotation_speed = 0,_inclination = 0;
	private: float _radius = 0,_orbit_inclination = 0,_orbit_speed = 0;
	private: std::vector<CelestialBody*> _children;

	

	public: CelestialBody(bonobo::mesh_data const& shape, GLuint const* program, GLuint diffuse_texture_id);


	public: glm::mat4 render(std::chrono::microseconds ellapsed_time, glm::mat4 const& view_projection, glm::mat4 const& parent_transform = glm::mat4(1.0f));

	void set_scale(glm::vec3 const& scale = glm::vec3(1,1,1));

	void set_spin(SpinConfiguration configuration);

	void set_orbit(OrbitConfiguration const& configuration);

	void set_ring(bonobo::mesh_data const& shape, GLuint const* program, GLuint diffuse_texture_id, glm::vec2 const& scale = glm::vec2(1.0f));

	void add_child(CelestialBody* child);

	std::vector<CelestialBody*> const& get_children() const;


};

