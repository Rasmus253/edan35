#include "interpolation.hpp"

glm::vec3
interpolation::evalLERP(glm::vec3 const& p0, glm::vec3 const& p1, float const x)
{
	glm::vec3 finalPos = glm::mat2x3(p0, p1) * glm::mat2(glm::vec2(1, 0), glm::vec2(-1, 1)) * glm::vec2(1, x);
	return finalPos;
}

glm::vec3
interpolation::evalCatmullRom(glm::vec3 const& p0, glm::vec3 const& p1,
	glm::vec3 const& p2, glm::vec3 const& p3,
	float const t, float const x)
{
	glm::vec3 finalPos = glm::mat4x3(p0, p1, p2, p3) * glm::mat4(
		glm::vec4(0, 1, 0, 0),
		glm::vec4(-t, 0, t, 0),
		glm::vec4(2 * t, t - 3, 3 - (2 * t), -t),
		glm::vec4(-t, 2 - t, t - 2, t)) *
		glm::vec4(1, x, pow(x, 2), pow(x, 3));

	return finalPos;
}
