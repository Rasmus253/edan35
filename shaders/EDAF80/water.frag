#version 410

uniform vec3 light_position;
uniform vec3 camera_position;
uniform samplerCube skybox;
uniform sampler2D normal_texture;

in VS_OUT {
vec3 vertex;
    vec3 normal;
    mat3 TBN;
    vec3 normalCoord0;
    vec3 normalCoord1;
    vec3 normalCoord2;
} fs_in;

out vec4 frag_color;

vec4 color_deep = vec4(0.0, 0.0, 0.1, 1.0);
vec4 color_shallow = vec4(0.0, 0.5, 0.5, 1.0);


void main()
{
    
    vec3 L = normalize(light_position - fs_in.vertex);
    vec3 V = normalize(camera_position - fs_in.vertex);

    vec3 n0 = 2.0 * texture(normal_texture, fs_in.normalCoord0.xy).xyz - 1.0;
    vec3 n1 = 2.0 * texture(normal_texture, fs_in.normalCoord1.xy).xyz - 1.0;
    vec3 n2 = 2.0 * texture(normal_texture, fs_in.normalCoord2.xy).xyz - 1.0;

    vec3 mapped_normal = fs_in.TBN * normalize(n0+n1+n2);


    vec3 reflection = reflect(-normalize(V), normalize(mapped_normal));

    
    float refract_const=0.02037;
    float fresnel=(refract_const+(1-refract_const)*pow(1-dot(V,normalize(mapped_normal)),5));

    float facing = 1 - max(dot(V,mapped_normal), 0);
    vec4 color_water = mix(color_deep, color_shallow, facing);

    float refract_index = 1/1.33;

    vec3 refraction=refract(V,normalize(mapped_normal),refract_index);

    vec3 reflection_final=reflection*fresnel+refraction*(1-fresnel);

    frag_color =color_water + texture(skybox, normalize(reflection_final));

}
