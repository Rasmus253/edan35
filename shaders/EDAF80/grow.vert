#version 410

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

uniform float ellapsed_time_s;
uniform mat4 vertex_model_to_world;
uniform mat4 normal_model_to_world;
uniform mat4 vertex_world_to_clip;

out VS_OUT {
	vec3 vertex;
	vec3 normal;
} vs_out;

void main() {

float growfactor = sin(ellapsed_time_s) + 1;
vec3 newV = vertex + normal * growfactor / 2;
gl_Position = vertex_world_to_clip * vertex_model_to_world * vec4(newV,1.0);
}

