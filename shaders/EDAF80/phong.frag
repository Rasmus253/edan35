#version 410

uniform vec3 light_position;
uniform vec3 camera_position;
uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform float shininess;
uniform int use_normal_mapping;

uniform int has_texture;
uniform int has_diffuse_texture;
uniform int has_opacity_texture;

uniform sampler2D diffuse_texture;
uniform sampler2D normal_texture;
uniform sampler2D rough_texture;


in VS_OUT {
    vec3 vertex;
    vec3 normal;
	mat3 TBN;
	vec3 textureCoord;
} fs_in;

out vec4 frag_color;

void main()
{
	vec3 mapped_normal = 2.0 * texture(normal_texture, fs_in.textureCoord.xy).xyz - 1.0;
	vec3 rough = texture(rough_texture, fs_in.textureCoord.xy).xyz;

	vec3 N;
	if(use_normal_mapping == 1){
		N = fs_in.TBN * normalize(mapped_normal);
	} else {
		N = normalize(fs_in.normal);
	}

    vec3 L = normalize(light_position - fs_in.vertex);
    vec3 V= normalize(camera_position-fs_in.vertex);

	

	vec4 diffuse_final = vec4(diffuse,1.0);
    if(has_diffuse_texture == 1) {
		diffuse_final = texture(diffuse_texture,fs_in.textureCoord.xy);
	} 
	diffuse_final = vec4(clamp(diffuse_final.rgb*dot(N,L),0.0,1.0),1.0);

    
    vec3 specular_final=rough*pow(clamp(dot(reflect(-L,N),V),0.0,1.0),shininess);
    frag_color = vec4(ambient+specular_final,1.0) +diffuse_final;
}
