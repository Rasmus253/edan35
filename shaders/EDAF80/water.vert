#version 410

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 binormal;

uniform mat4 vertex_model_to_world;
uniform mat4 normal_model_to_world;
uniform mat4 vertex_world_to_clip;
uniform float ellapsed_time_s;
uniform vec3 camera_position;

vec2 texScale = vec2(8,4);
float normalTime = mod(ellapsed_time_s,100);
vec2 normalSpeed = vec2(-0.05,0);
vec3 normalCoord0 = vec3(0,0,0);
vec3 normalCoord1 = vec3(0,0,0);
vec3 normalCoord2 = vec3(0,0,0);


out VS_OUT {
	vec3 vertex;
	vec3 normal;
	mat3 TBN;
	vec3 normalCoord0;
	vec3 normalCoord1;
	vec3 normalCoord2;
} vs_out;

float wave(vec2 position, vec2 direction, float amplitude, float frequency, float phase, float sharpness, float time) {
	return amplitude * pow(sin((position.x * direction.x + position.y * direction.y) * frequency + time * phase) * 0.5 + 0.5, sharpness);
}

float dx_wave(vec2 position, vec2 direction, float amplitude, float frequency, float phase, float sharpness, float time) {
	return 0.5 * sharpness * direction.x * frequency * amplitude * pow(sin((position.x * direction.x + position.y * direction.y) * frequency + time * phase) * 0.5 + 0.5, sharpness - 1) * cos((position.x * direction.x + position.y * direction.y) * frequency + time * phase);
}

float dy_wave(vec2 position, vec2 direction, float amplitude, float frequency, float phase, float sharpness, float time) {
	return 0.5 * sharpness * direction.y * frequency * amplitude * pow(sin((position.x * direction.x + position.y * direction.y) * frequency + time * phase) * 0.5 + 0.5, sharpness - 1) * cos((position.x * direction.x + position.y * direction.y) * frequency + time * phase);
}


void main()
{
	vec3 displaced_vertex = (vertex_model_to_world * vec4(vertex,1.0)).xyz;
	float combined_wave = wave(vertex.xz, vec2(-1.0, 0.0), 1.0, 0.4, 0.5, 2.5, ellapsed_time_s) + wave(vertex.xz, vec2(-0.7, 0.7), 0.5, 0.6, 1.3, 2.0, ellapsed_time_s);
	float combined_dx_wave = dx_wave(vertex.xz, vec2(-1.0, 0.0), 1.0, 0.4, 0.5, 2.5, ellapsed_time_s) + dx_wave(vertex.xz, vec2(-0.7, 0.7), 0.5, 0.6, 1.3, 2.0, ellapsed_time_s);
	float combined_dy_wave = dy_wave(vertex.xz, vec2(-1.0, 0.0), 1.0, 0.4, 0.5, 2.5, ellapsed_time_s) + dy_wave(vertex.xz, vec2(-0.7, 0.7), 0.5, 0.6, 1.3, 2.0, ellapsed_time_s);

	displaced_vertex.y += combined_wave;

	vec3 V = normalize(camera_position - displaced_vertex);

	vec3 t = vec3(1,combined_dx_wave,0);
	vec3 b = vec3(0,combined_dy_wave,1);
	vec3 n = vec3(-combined_dx_wave, 1, -combined_dy_wave);

	vec3 N = normalize((normal_model_to_world * vec4(n, 0.0)).xyz);
	vec3 T = normalize((normal_model_to_world * vec4(tangent, 0.0)).xyz);
	vec3 B = normalize((normal_model_to_world * vec4(binormal, 0.0)).xyz);
	vs_out.TBN = mat3(T,B,N);


	normalCoord0.xy = texCoords.xy*texScale   + normalTime*normalSpeed;
	normalCoord1.xy = texCoords.xy*texScale*2 + normalTime*normalSpeed*4;
	normalCoord2.xy = texCoords.xy*texScale*4 + normalTime*normalSpeed*8;


	vs_out.vertex = displaced_vertex;
	vs_out.normal = vec3(normal_model_to_world * vec4(normal, 0.0));
	vs_out.normalCoord0 = normalCoord0;
	vs_out.normalCoord1 = normalCoord1;
	vs_out.normalCoord2 = normalCoord2;

	gl_Position = vertex_world_to_clip * vertex_model_to_world * vec4(displaced_vertex, 1.0);
}



