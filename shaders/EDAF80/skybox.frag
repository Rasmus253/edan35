#version 410

uniform vec3 light_position;
uniform vec3 camera_position;

in VS_OUT {
	vec3 vertex;
}fs_in;

out vec4 FragColor;
uniform samplerCube skybox;

void main()
{
	vec3 L = normalize(fs_in.vertex - camera_position);
    FragColor = texture(skybox, L);
}
