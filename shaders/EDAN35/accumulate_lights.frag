#version 410

uniform sampler2D depth_texture;
uniform sampler2D normal_texture;
uniform sampler2DShadow shadow_texture;

uniform vec2 inv_res;

uniform mat4 view_projection_inverse;
uniform vec3 camera_position;
uniform mat4 shadow_view_projection;

uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 light_direction;
uniform float light_intensity;
uniform float light_angle_falloff;

uniform vec2 shadowmap_texel_size;

layout (location = 2) in vec4 geometry_normal;


layout (location = 0) out vec4 light_diffuse_contribution;
layout (location = 1) out vec4 light_specular_contribution;


void main()
{

	vec2 texCoord = gl_FragCoord.xy * inv_res;

	//vec3 normal = 2*texture(normal_texture,texCoord).xyz - 1;
	vec3 normal = 2*geometry_normal.xyz - 1;

	vec4 depth = 2*texture(depth_texture,texCoord) - 1;

	vec4 position = view_projection_inverse * depth;
	position /= depth.w;


	vec3 view_direction= normalize(camera_position-position.xyz);

	light_diffuse_contribution  = 0.5*vec4(clamp(light_color.rgb*dot(normal,light_direction),0.0,1.0),1.0)+0.5;
	light_specular_contribution = 0.5*vec4(pow(clamp(dot(reflect(-light_direction,normal),view_direction),0.0,1.0),0.5))+0.5;
}
